const protocol = require('http'); 
const port = 4000;

//create connection
protocol.createServer((req, res) => {
	res.write(`Welcome to the Server`);
	res.end()
}).listen()
//listen() to bind the connection to port

console.log(`Server is running on port ${port}`)